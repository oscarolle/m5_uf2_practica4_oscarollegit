package calculadora;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

class LaCalculadoraTest {

	@Test
	void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30,  res, "La suma no es correcta!");

	//	fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = LaCalculadora.resta(50, 20);
		assertEquals(40,  res, "La resta no es correcta!");
	//	fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(5, 4);
		assertEquals(20,  res, "La multiplicaci� no es correcta!");
	//	fail("Not yet implemented");
	}

	@Test
	void testDivisio() {
		int res = LaCalculadora.divisio(10, 5);
		assertEquals(2,  res, "La divisi� no es correcta!");
	//	fail("Not yet implemented");
	}

	@Test
	void testDivideix() {
		int res = LaCalculadora.divideix(12, 0);
		assertEquals(2,  res, "La divisi� no es correcta!");
	//	fail("Not yet implemented");
	}
	
	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			fail("Fallo: Passa per aqu� si no es llan�a l�excepci� ArithmeticException");
		}
		catch (ArithmeticException e) {
			System.out.print("La prova funciona correctament\n");
		//La prova funciona correctament
		}
    }

	@Test
	void testExpectedException() {
	 
	  Assertions.assertThrows(ArithmeticException.class, () -> {
		  LaCalculadora.divideix(10, 0);
	  });
	 
	}

	
}
